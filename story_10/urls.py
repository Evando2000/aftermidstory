from django.urls import path, include
from . import views

app_name = 'story_10'
urlpatterns = [
    path('', views.loadbal_view, name='load_balancer'),
]