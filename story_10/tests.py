from django.test import TestCase, Client
from django.urls import resolve
from story_10.views import loadbal_view

class Story10UnitTest(TestCase):
    def test_url_salah(self):
        c = Client()
        response = c.get('/next')
        self.assertEqual(response.status_code, 404)

    def test_url(self):
        c = Client()
        response = c.get('/story10/')
        self.assertEqual(response.status_code, 200)
    
    def test_view_function(self):
        response = resolve('/story10/')
        self.assertEqual(response.func, loadbal_view)

    def test_use_template(self):
        c = Client()
        response = c.get('/story10/')
        self.assertTemplateUsed(response, 'load_bal.html')