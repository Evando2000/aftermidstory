from django.test import TestCase, Client
from django.urls import resolve
from story_9.views import register_view, login_view, logout_view, restricted_view
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

class Story9UnitTest(TestCase):
    def test_url_default(self):
        c = Client()
        response = c.get('/story9/login/?next=/story9/')
        self.assertEqual(response.status_code, 200)

    def test_url_register(self):
        c = Client()
        response = c.get('/story9/register/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_login(self):
        c = Client()
        response = c.get('/story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_url_logout(self):
        c = Client()
        response = c.get('/story9/logout/')
        self.assertEqual(response.status_code, 200)

    def test_view_restricted(self):
        response = resolve('/story9/')
        self.assertEqual(response.func, restricted_view)

    def test_view_register(self):
        response = resolve('/story9/register/')
        self.assertEqual(response.func, register_view)

    def test_view_login(self):
        response = resolve('/story9/login/')
        self.assertEqual(response.func, login_view)

    def test_view_logout(self):
        response = resolve('/story9/logout/')
        self.assertEqual(response.func, logout_view)

    def test_use_template_default(self):
        c = Client()
        response = c.get('/story9/login/?next=/story9/')
        self.assertTemplateUsed(response, 'login.html')

    def test_use_template_register(self):
        c = Client()
        response = c.get('/story9/register/')
        self.assertTemplateUsed(response, 'register.html')

    def test_use_template_login(self):
        c = Client()
        response = c.get('/story9/login/')
        self.assertTemplateUsed(response, 'login.html')

    def setUp(self):
        user = User.objects.create_user(username="tester1",password="ing1nLulus")
        user.save()

    def test_register(self):
        response = self.client.post('/story9/register/',data={
            'username':"pengenLulus",
            'first_name':"Saya", 
            'last_name':"Ingin", 
            'email':"cepatlulus@gmail.com",
            'password1':'amin1290',
            'password2': 'amin1290',
        })
        user = User.objects.all()
        self.assertEqual(len(user),2)
        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code,302)
        self.assertRedirects(expected_url='/story9/',response=response)

    def test_login(self):
        response = self.client.post('/story9/login/',data={
            'username':"tester1",
            'password':'ing1nLulus'
        })

        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(expected_url='/story9/',response=response)

    def test_logout(self):
        response = self.client.post('/story9/login/',data={
            'username':"tester1",
            'password':'ing1nLulus'
        })

        response = self.client.post('/story9/logout/')
        self.assertNotIn('_auth_user_id',self.client.session)
        self.assertNotIn('_auth_user_backend', self.client.session)
        self.assertNotIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(expected_url='/story9/login/',response=response)
