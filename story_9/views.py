from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from story_9.forms import RegisterForm, LoginForm

def register_view(request):
    if request.method == 'POST':
        form_regis = RegisterForm(request.POST)
        if form_regis.is_valid():
            form_regis.save()
            username = form_regis.cleaned_data.get('username')
            raw_password = form_regis.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/story9/')
    else:
        form_regis = RegisterForm()
    return render(request, "register.html", {'form': form_regis})

def login_view(request):
    if request.user.is_authenticated:
        return redirect('/story9/')
    else:
        form = LoginForm()
        if request.method == "POST":
            form = LoginForm(request=request,data=request.POST)
            if form.is_valid():
                user = form.get_user()
                login(request,user=user)
                if 'next' in request.POST:
                    return redirect(request.POST.get('next'))
                return redirect('/story9/')
            else:
                return render(request, "login.html", {'form': form})
        return render(request, "login.html", {'form': form})

def logout_view(request):
    if request.method == "POST":
        logout(request)
        return redirect('/story9/login/')
    return HttpResponse()

@login_required(login_url='/story9/login')
def restricted_view(request):
    return render(request, "restricted.html")