$(document).ready(() =>{
  $.ajax({
    method: 'GET',
    url: 'https://www.googleapis.com/books/v1/volumes?q=bumi', 
    success: function(result){
      $("#buku-ini").empty();
      $("#buku-ini").append(
      '<thead class="thead-dark">'+
          '<tr>'+
              '<th scope="col">ID Buku</th>' +
              '<th scope="col">Judul Buku</th>' +
              '<th scope="col">Penulis</th>' +
              '<th scope="col">Penerbit</th>' +
              '<th scope="col">Deskripsi Buku</th>' +
              '<th scope="col">Gambar Buku</th>' +
          '</tr>' +
      '</thead>'
      );

      for(i=0; i < result.items.length ; i++) {
        let id_buku = result.items[i].id;
        let title_buku = result.items[i].volumeInfo.title;
        let authors = result.items[i].volumeInfo.authors;
        let publisher = result.items[i].volumeInfo.publisher;
        let desc_buku = result.items[i].volumeInfo.description;
        let gambar_buku = result.items[i].volumeInfo.imageLinks.thumbnail;

        if(id_buku == undefined){
          id_buku = "No ID"
        }

        if(title_buku == undefined){
          title_buku = "No Title"
        }

        if(authors == undefined){
          authors = "No Authors"
        }

        if(publisher == undefined){
          publisher = "No Publisher"
        }

        if(desc_buku == undefined){
          desc_buku = "No Description"
        }

        if(gambar_buku == undefined){
          gambar_buku = "No Image"
        }
        
        $("#buku-ini").append(
          "<tr>" +
            "<td>" + id_buku + "</td>" + 
            "<td>" + title_buku + "</td>" + 
            "<td>" + authors + "</td>" + 
            "<td>" + publisher + "</td>" + 
            "<td>" + desc_buku + "</td>" + 
            "<td>" + "<img src= '" + gambar_buku + "'>" + "</td>" + 
          "</tr>");
      }
    }
  });

  $("#search-box-input").on("change",
    function(){
      let inputan = $("#search-box-input").val()
      $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q='+inputan, 
        success: function(result){
          $("#buku-ini").empty();
          $("#buku-ini").append(
          '<thead class="thead-dark">'+
              '<tr>'+
                  '<th scope="col">ID Buku</th>' +
                  '<th scope="col">Judul Buku</th>' +
                  '<th scope="col">Penulis</th>' +
                  '<th scope="col">Penerbit</th>' +
                  '<th scope="col">Deskripsi Buku</th>' +
                  '<th scope="col">Gambar Buku</th>' +
              '</tr>' +
          '</thead>'
          );
          for(i=0; i < result.items.length ; i++) {
            let id_buku = result.items[i].id;
            let title_buku = result.items[i].volumeInfo.title;
            let authors = result.items[i].volumeInfo.authors;
            let publisher = result.items[i].volumeInfo.publisher;
            let desc_buku = result.items[i].volumeInfo.description;
            let gambar_buku = result.items[i].volumeInfo.imageLinks.thumbnail;

            if(id_buku == undefined){
              id_buku = "No ID"
            }

            if(title_buku == undefined){
              title_buku = "No Title"
            }

            if(authors == undefined){
              authors = "No Authors"
            }

            if(publisher == undefined){
              publisher = "No Publisher"
            }

            if(desc_buku == undefined){
              desc_buku = "No Description"
            }

            if(gambar_buku == undefined){
              gambar_buku = "No Image"
            }
            
            $("#buku-ini").append(
              "<tr>" +
                "<td>" + id_buku + "</td>" + 
                "<td>" + title_buku + "</td>" + 
                "<td>" + authors + "</td>" + 
                "<td>" + publisher + "</td>" + 
                "<td>" + desc_buku + "</td>" + 
                "<td>" + "<img src= '" + gambar_buku + "'>" + "</td>" + 
              "</tr>");
          }
        }
      });
    }
  );

  $("#search-button").on("click",
    function(){
      let inputan = $("#search-box-input").val()
      $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q='+inputan, 
        success: function(result){
          $("#buku-ini").empty();
          $("#buku-ini").append(
          '<thead class="thead-dark">'+
              '<tr>'+
                  '<th scope="col">ID Buku</th>' +
                  '<th scope="col">Judul Buku</th>' +
                  '<th scope="col">Penulis</th>' +
                  '<th scope="col">Penerbit</th>' +
                  '<th scope="col">Deskripsi Buku</th>' +
                  '<th scope="col">Gambar Buku</th>' +
              '</tr>' +
          '</thead>'
          );
          for(i=0; i < result.items.length ; i++) {
            let id_buku = result.items[i].id;
            let title_buku = result.items[i].volumeInfo.title;
            let authors = result.items[i].volumeInfo.authors;
            let publisher = result.items[i].volumeInfo.publisher;
            let desc_buku = result.items[i].volumeInfo.description;
            let gambar_buku = result.items[i].volumeInfo.imageLinks.thumbnail;

            if(id_buku == undefined){
              id_buku = "No ID"
            }

            if(title_buku == undefined){
              title_buku = "No Title"
            }

            if(authors == undefined){
              authors = "No Authors"
            }

            if(publisher == undefined){
              publisher = "No Publisher"
            }

            if(desc_buku == undefined){
              desc_buku = "No Description"
            }

            if(gambar_buku == undefined){
              gambar_buku = "No Image"
            }
            
            $("#buku-ini").append(
              "<tr>" +
                "<td>" + id_buku + "</td>" + 
                "<td>" + title_buku + "</td>" + 
                "<td>" + authors + "</td>" + 
                "<td>" + publisher + "</td>" + 
                "<td>" + desc_buku + "</td>" + 
                "<td>" + "<img src= '" + gambar_buku + "'>" + "</td>" + 
              "</tr>");
          }
        }
      });
    }
  );
})