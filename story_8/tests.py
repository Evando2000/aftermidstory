from django.test import TestCase, Client
from django.urls import resolve
from story_8.views import home

class Story8UnitTest(TestCase):
    def test_url_salah(self):
        c = Client()
        response = c.get('/next')
        self.assertEqual(response.status_code, 404)

    def test_url(self):
        c = Client()
        response = c.get('/story8/')
        self.assertEqual(response.status_code, 200)
    
    def test_view_function(self):
        response = resolve('/story8/')
        self.assertEqual(response.func, home)

    def test_use_template(self):
        c = Client()
        response = c.get('/story8/')
        self.assertTemplateUsed(response, 'ajax_json.html')