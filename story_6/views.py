from django.shortcuts import render, redirect
from . import forms
from .models import Status

def daftar_status(request):
    if request.method == 'POST':
        form = forms.BuatStatus(request.POST)
        if form.is_valid():
            form.save()
            return redirect ('story_6:view_form')
    else:
        form = forms.BuatStatus()

    daftar_status = Status.objects.all().order_by('-date_created')
    return render(request, "landing_page.html", {'semua_status':daftar_status, 'form':form})