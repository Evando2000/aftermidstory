from django import forms
from . import models

class BuatStatus(forms.ModelForm):
    class Meta:
        model = models.Status
        fields = ['status_inputan']