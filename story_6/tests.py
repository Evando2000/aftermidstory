from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from aftermid.settings import BASE_DIR
import os
from story_6.models import Status
from story_6.views import daftar_status

class Story6FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome(os.path.join(BASE_DIR, "chromedriver"), chrome_options=chrome_options)
        super(Story6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

    def test_welcome_text(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        # find the form element
        welcome = selenium.find_element_by_id('welcome_text')

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        # find the form element
        form_in = selenium.find_element_by_id('id_status_inputan')
        submit = selenium.find_element_by_id('btn_submit')

        # Fill the form with data
        # maks length 300
        form_in.send_keys('Coba Coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)
        self.assertIn('Coba Coba', selenium.page_source)

class Story6UnitTest(TestCase):
    def test_url_salah(self):
        c = Client()
        response = c.get('/next')
        self.assertEqual(response.status_code, 404)

    def test_url(self):
        c = Client()
        response = c.get('')
        self.assertEqual(response.status_code, 200)
    
    def test_view_function(self):
        response = resolve('/')
        self.assertEqual(response.func, daftar_status)

    def test_use_template(self):
        c = Client()
        response = c.get('')
        self.assertTemplateUsed(response, 'landing_page.html')

    def test_form_invalid(self):
        c = Client()
        status_failed = {
            #panjang melebihi maksimum
            'status_inputan': "a"*301
        }
        response = c.post('/', data=status_failed)

        status_size = Status.objects.all().count()
        self.assertEqual(status_size, 0)
        self.assertEqual(response.status_code,200)

    def test_form_valid(self):
        c = Client()
        add_status = {
            #panjang melebihi maksimum
            'status_inputan': "a"*300
        }
        response = c.post('/', data=add_status)

        semua_status = Status.objects.all()
        self.assertEqual(len(semua_status), 1)        
        self.assertEqual(semua_status[0].status_inputan, "a"*300)
    
    def test_def_models(self):
        status_test = Status.objects.create(status_inputan="Testing")
        self.assertEqual(str(status_test),"Testing")
