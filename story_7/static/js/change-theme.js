$(document).on("change",".switch",function() { 
    $(".container-fluid").toggleClass("dark");
    $(".accord-header").toggleClass("dark");
    $(".accord-content").toggleClass("dark");
});

$( function() {
    $( "#accordion" ).accordion({
        collapsible: true,
        active: false
    });
});