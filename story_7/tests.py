from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from aftermid.settings import BASE_DIR
import os
from story_7.views import home

class Story7UnitTest(TestCase):
    def test_url_salah(self):
        c = Client()
        response = c.get('/next')
        self.assertEqual(response.status_code, 404)

    def test_url(self):
        c = Client()
        response = c.get('/story7/')
        self.assertEqual(response.status_code, 200)
    
    def test_view_function(self):
        response = resolve('/story7/')
        self.assertEqual(response.func, home)

    def test_use_template(self):
        c = Client()
        response = c.get('/story7/')
        self.assertTemplateUsed(response, 'javas.html')